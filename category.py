# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields

__all__ = ['CardCategory', 'CardCategoryBenefit']


class CardCategory(ModelSQL, ModelView):
    "Card Category"
    __name__ = "party.card.category"
    name = fields.Char('Name', required=True)
    kind = fields.Selection([
        ('cash', 'Cash'),
        ('points', 'Points'),
        ('products', 'Products'),
        ], 'Kind', select=True, required=True)
    amount = fields.Numeric('Amount')
    expire_time = fields.Integer('Expire Time', help='In days')
    benefits = fields.One2Many('party.card.category_benefit',
        'category', 'Card Category Benefit')

    @classmethod
    def __setup__(cls):
        super(CardCategory, cls).__setup__()
        cls._order.insert(0, ('name', 'ASC'))


class CardCategoryBenefit(ModelSQL, ModelView):
    'Card Category Benefit'
    __name__ = 'party.card.category_benefit'
    category = fields.Many2One('party.card.category', 'Category Card',
        ondelete='CASCADE', select=True, required=True)
    product = fields.Many2One('product.product', 'Product', select=True,
        required=True)
    quantity = fields.Integer('Quantity', required=True)
    description = fields.Char('Description')

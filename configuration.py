# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


__all__ = ['PartyConfiguration']


class PartyConfiguration:
    __metaclass__ = PoolMeta
    __name__ = 'party.configuration'
    party_card_sequence = fields.MultiValue(fields.Many2One('ir.sequence',
        'Card Sequence', required=True, domain=[
                ('code', '=', 'party.card')])
                )
    points_value = fields.Numeric('Points Value')

# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from datetime import timedelta

from trytond.model import ModelView, ModelSQL, Workflow, fields
from trytond.pyson import Eval
from trytond.pool import Pool
from trytond.rpc import RPC

__all__ = ['Card', 'CardConsumption', 'CardBenefit']

STATES = {'readonly': (Eval('state') != 'draft'),}

_ZERO = Decimal('0.0')


class Card(Workflow, ModelSQL, ModelView):
    'Party Card'
    __name__ = 'party.card'
    number = fields.Char('Number', select=True, states={
            'readonly': True,
        })
    serial = fields.Char('Serial', required=True, select=True,
        states={
            'readonly': True,
        })
    party = fields.Many2One('party.party', 'Party',
        required=True, states={
            'readonly': Eval('state') != 'draft',
        })
    release_date = fields.Date('Release Date', states=STATES,
        required=True)
    category = fields.Many2One('party.card.category', 'Card Category',
        required=True, states={
            'readonly': Eval('state') != 'draft',
        })
    expire_date = fields.Date('Expire Date', states=STATES,
        required=True)
    points_balance = fields.Integer('Points Balance', states={
        'readonly': (Eval('state') != 'draft'),
        'invisible': (Eval('kind_category') != 'points'),
        },  select=True)
    cash_balance= fields.Numeric('Cash Balance', digits=(16, 2),
        states={
            'readonly': (Eval('state') != 'draft'),
            'invisible': (Eval('kind_category') != 'cash'),
        },  select=True)
    consumptions = fields.One2Many('party.card.consumption', 'card',
        'Cosumptions', states=STATES, depends=['state'])
    benefits = fields.One2Many('party.card.benefit', 'card',
        'Benefits', depends=['state'],  states={
            'readonly': True,
        })
    state = fields.Selection([
            ('draft', 'Draft'),
            ('released', 'Released'),
            ('cancelled', 'Cancelled'),
            ('expired', 'Expired'),
        ], 'State', readonly=True)
    kind_category = fields.Function(fields.Char('Kind Category',
        depends=['category']), 'get_kind_category')

    @classmethod
    def __setup__(cls):
        super(Card, cls).__setup__()
        cls._order.insert(0, ('release_date', 'DESC'))
        cls._transitions |= set((
            ('draft', 'cancelled'),
            ('cancelled', 'draft'),
            ('draft', 'released'),
            ('released', 'draft'),
            ('released', 'expired'),
            ('released', 'cancelled'),
        ))
        cls._buttons.update({
            'draft': {
                'invisible': Eval('state') == 'draft',
                },
            'release': {
                'invisible': Eval('state') != 'draft',
                },
            'cancel': {
                'invisible': ~Eval('state').in_(['draft', 'released']),
                },
            'expired': {
                'invisible': Eval('state') != 'released',
                },
        })
        cls._disp = False
        cls.__rpc__.update({
            'activate_card': RPC(readonly=False, instantiate=0),
            'get_number': RPC(readonly=True, instantiate=0),
            'add_consumption': RPC(readonly=False, instantiate=0),
        })
        cls._error_messages.update({
            'sequence_missing': ('The party card sequence is missing!'),
        })

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_release_date():
        return Pool().get('ir.date').today()

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('cancelled')
    def cancel(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('released')
    def release(cls, records):
        for record in records:
            record.add_benefits()

    @classmethod
    @ModelView.button
    @Workflow.transition('expired')
    def expired(cls, records):
        pass

    @fields.depends('expire_date', 'release_date')
    def on_change_release_date(self, name=None):
        if self.release_date:
            self.expire_date = self._get_expire_date(self.release_date)

    @classmethod
    def _get_expire_date(cls, start_date):
         return start_date + timedelta(365)

    def get_king_category(self, name=None):
        if self.category:
            return self.category.kind

    def add_benefits(self):
        Benefit = Pool().get('party.card.benefit')
        benefit_to_create = []
        if self.benefits:
            return
        for benefit in self.category.benefits:
            benefit_to_create.append({
                'card': self.id,
                'product': benefit.product.id,
                'quantity': benefit.quantity,
            })
        Benefit.create(benefit_to_create)

    @classmethod
    def activate_card(cls, ids, party_id, category_id, serial):
        if not cls._validate_serial(serial):
            return
        today = Pool().get('ir.date').today()
        new_card, = cls.create([{
            'party': party_id,
            'serial': serial,
            'category': category_id,
            'release_date': today,
            'expire_date': cls._get_expire_date(today),
            'state': 'released',
        }])
        new_card.set_number()
        new_card.add_benefits()
        new_card.save()
        return new_card.number

    @classmethod
    def _validate_serial(cls, serial):
        res = cls.search([
            ('serial', '=', serial)
        ])
        if res:
            return False
        else:
            return True

    @classmethod
    def get_number(cls, serial_id):
        number = None
        return number

    @classmethod
    def add_consumption(cls, card_ids, product_id, quantity=1):
        card_id = card_ids[0]
        pool = Pool()
        today = Pool().get('ir.date').today()
        Consumption = pool.get('party.card.consumption')

        consumption, = Consumption.create([{
            'card': card_id,
            'date_consumption': today,
            'product': product_id,
            'quantity': quantity,
        }])
        return consumption.id

    def set_number(self):
        if self.number:
            return
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Configuration = pool.get('party.configuration')
        configuration = Configuration(1)
        if not configuration.party_card_sequence:
            self.raise_user_error('sequence_missing',)
        seq = configuration.party_card_sequence.id
        self.write([self], {'number': Sequence.get_id(seq)})


class CardConsumption(ModelSQL, ModelView):
    "Card Consumption"
    __name__ = "party.card.consumption"
    card = fields.Many2One('party.card', 'Card', ondelete='CASCADE',
            select=True, required=True)
    product = fields.Many2One('product.product', 'Product',
        domain=[('salable', '=', True)])
    date_consumption = fields.Date('Date', required=True)
    amount = fields.Numeric('Amount')
    points = fields.Integer('Points')
    quantity = fields.Integer('Quantity')

    @classmethod
    def __setup__(cls):
        super(CardConsumption, cls).__setup__()
        cls._order.insert(0, ('date_consumption', 'DESC'))
        cls._disp = False
        cls.__rpc__.update({
            'remove_product': RPC(readonly=False, instantiate=0),
        })

    @staticmethod
    def default_date_consumption():
        return Pool().get('ir.date').today()

    @classmethod
    def remove_product(cls, benefit_ids):
        benefits = cls.browse(benefit_ids)
        cls.delete(benefits)


class CardBenefit(ModelSQL, ModelView):
    "Card Benefit"
    __name__ = "party.card.benefit"
    card = fields.Many2One('party.card', 'Card', ondelete='CASCADE',
            select=True, required=True)
    product = fields.Many2One('product.product', 'Product',
        domain=[('salable', '=', True)], states={
            'readonly': True,
        })
    quantity = fields.Integer('Quantity', states={
            'readonly': True,
        }, required=True)
    balance = fields.Function(fields.Integer('Balance'), 'get_balance')

    def get_balance(self, name=None):
        balance = self.quantity
        for consumption in self.card.consumptions:
            if not consumption.product:
                continue
            if self.product.id == consumption.product.id and consumption.quantity:
                balance -= consumption.quantity
        return balance

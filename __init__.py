# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from .card import (Card, CardConsumption, CardBenefit)
from .category import (CardCategory, CardCategoryBenefit)
from .configuration import PartyConfiguration

def register():
    Pool.register(
        PartyConfiguration,
        CardCategory,
        Card,
        CardCategoryBenefit,
        CardConsumption,
        CardBenefit,
        module='party_card', type_='model')
